const mix = require('laravel-mix');
require('laravel-mix-jigsaw');

mix.disableSuccessNotifications();
mix.setPublicPath('source/assets/build');

mix.copy('source/_assets/fonts', 'source/assets/build/fonts');

mix.sass('source/_assets/scss/fonts.scss', 'css/fonts.css', {
    sassOptions: {outputStyle: 'compressed'}
});

mix.jigsaw({
    browserSync: false,
}).sass('source/_assets/scss/app.scss', 'css/app.css', {}, [
    require('postcss-import'),
    require('tailwindcss'),
]).options({
    processCssUrls: false,
}).js('source/_assets/js/stats.js', 'scripts').version();
