const colors = require('./colours')
const forms = require('@tailwindcss/forms')
const ratio = require('@tailwindcss/aspect-ratio')
const autoprefixer = require('autoprefixer')
const typography = require('@tailwindcss/typography')

module.exports = {
    content: [
        'source/**/*.php',
    ],
    theme: {
        colors: {
            primary: colors.primary,
            neutral: colors.neutral,
            error: colors.error,
            warning: colors.warning,
            success: colors.success,
            info: colors.info,
            black: colors.black,
            white: colors.white,
            transparent: colors.transparent,
        },
        extend: {
            skew: {
                '-20': '-20deg'
            },
            fontFamily: {
                sans: ['Raleway', 'ui-sans-serif', 'system-ui', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Helvetica Neue', 'Arial', 'Noto Sans', 'sans-serif', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'],
            }
        },
    },
    plugins: [
        forms,
        ratio,
        autoprefixer,
        typography,
    ],
};
