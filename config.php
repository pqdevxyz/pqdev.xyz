<?php

return [
    'production' => false,
    'baseUrl' => 'https://dev.pqdev.xyz',
    'title' => 'Paul Quine - Software Engineer',
    'description' => 'I’m an enthusiastic web developer with a passion for building bespoke solutions, incorporating the latest trends in design and development.',
];
