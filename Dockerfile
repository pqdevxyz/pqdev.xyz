FROM 339898008043.dkr.ecr.eu-west-2.amazonaws.com/roi-skimmed:latest

COPY ./ /var/www/html

COPY ./docker/boot-container.sh /usr/local/bin/boot-container
COPY ./docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./docker/nginx/main.conf /etc/nginx/nginx.conf
COPY ./docker/nginx/pqdev.conf /etc/nginx/sites-enabled/

RUN chmod +x /usr/local/bin/boot-container
RUN chmod +x ./docker/setup-container.sh

EXPOSE 80

ENTRYPOINT ["boot-container"]
