exports.handler = (event, context, callback) => {
    const response = {
        statusCode: 302,
        headers: {
            Location: 'https://pqdev.xyz',
        }
    };

    return callback(null, response);
}
