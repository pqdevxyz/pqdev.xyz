<div class="w-full grid grid-cols-1 grid-rows-1">
    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10">
        <div class="bg-gradient-to-l from-primary-800 to-primary-500 transform-gpu h-full origin-center -skew-y-3"></div>
    </div>
    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
        <div class="container mx-auto">
            <div class="grid xl:grid-cols-3 grid-cols-1 gap-8 px-4 sm:px-20 py-28 xl:gap-4">
                <div class="flex items-center justify-center">
                    <div class="text-center">
                        <h4 class="font-sans text-3xl font-extrabold text-white mb-4">Terraform (IaC)</h4>
                        <p class="font-sans text-md font-medium text-white mb-4">A project for importing my infrastructure into Terraform, including my DR / fail over process.</p>
                        <a href="/terraform" class="font-sans text-md font-medium text-white hover:text-neutral-700 umami--click--view-terraform">View details</a>
                    </div>
                </div>
                <div class="flex items-center justify-center">
                    <div class="text-center">
                        <h4 class="font-sans text-3xl font-extrabold text-white mb-4">AWS 2 Slack</h4>
                        <p class="font-sans text-md font-medium text-white mb-4">I have been building lambda functions that send certain AWS EventBridge messages to slack.</p>
                        <a href="https://bitbucket.org/pqdevxyz/awstoslack" class="font-sans text-md font-medium text-white hover:text-neutral-700 umami--click--view-react-demo-code">View the code</a>
                    </div>
                </div>
                <div class="flex items-center justify-center">
                    <div class="text-center">
                        <h4 class="font-sans text-3xl font-extrabold text-white mb-4">Micro Chess Game</h4>
                        <p class="font-sans text-md font-medium text-white mb-4">A chess game built for my A-Level computing project, written in C#.</p>
                        <a href="https://bitbucket.org/pqdevxyz/micro-chess-game" class="font-sans text-md font-medium text-white hover:text-neutral-700 umami--click--view-micro-chess-code">View the code</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
