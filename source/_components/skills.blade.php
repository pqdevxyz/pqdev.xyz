<div class="w-full grid grid-cols-1 grid-rows-1">
    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10">
        <div class="bg-gradient-to-r from-primary-800 to-primary-500 transform-gpu h-full origin-center skew-y-3"></div>
    </div>
    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
        <div class="container px-4 sm:px-8 mx-auto">
            <div class="py-28">
                <div class="grid grid-cols-1 lg:grid-cols-3 gap-4 mb-4 text-center lg:text-left">
                    <div class="lg:col-start-1 lg:col-end-4">
                        <h3 class="font-sans text-3xl font-bold text-white mb-4">Development Skills</h3>
                    </div>
                    <div>
                        <h4 class="font-sans text-2xl font-semibold text-white mb-2">Strong Knowledge Of</h4>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Laravel</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">PHP</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">SQL</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Redis</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">AWS</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Linux</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Bitbucket Pipelines</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Bootstrap & Tailwind</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">SCSS</p>
                    </div>
                    <div>
                        <h4 class="font-sans text-2xl font-semibold text-white mb-2">Good Experience Using</h4>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">PHPUnit</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">LaraStan / PHPStan (Static Code Analysis)</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Cypress</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Git</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Nginx</p>
                    </div>
                    <div>
                        <h4 class="font-sans text-2xl font-semibold text-white mb-2">Continuing To Learn</h4>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">React</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Angular</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Docker inc Kubernetes</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Terraform</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Laravel Octane & Swoole</p>
                    </div>
                </div>
                <div class="grid grid-cols-1 lg:grid-cols-3 gap-4 text-center lg:text-left">
                    <div>
                        <h3 class="font-sans text-3xl font-bold text-white mb-4">IT Skills</h3>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">Adobe Photoshop, XD & Illustrator</p>
                        <p class="font-sans text-lg font-medium text-white leading-relaxed">JIRA & Confluence</p>
                    </div>
                </div>
                <div class="hidden md:grid grid-cols-1 gap-4">
                    <div>
                        <h3 class="font-sans text-3xl font-bold text-white mb-4">&nbsp;</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
