<div class="w-full bg-center bg-cover bg-no-repeat home-myroi-banner">
    <div class="text-center px-4 sm:px-20 py-24 lg:py-36">
        <h2 class="font-sans text-4xl font-black mb-12 text-white">MyROI</h2>
        <h4 class="font-sans text-2xl font-semibold mb-12 text-white">Track your investments.</h4>
        <p class="font-sans text-xl font-medium mb-12 text-white">A project for keeping track of investments for small to medium size portfolios. It uses caching to improve performance when fetching data for more complex calculations.</p>
        <a href="/myroi" class="font-sans text-md font-medium text-white hover:text-primary-500 umami--click--view-myroi">View details</a>
    </div>
</div>
