<div class="w-full bg-center bg-cover bg-no-repeat home-vaultstack-banner">
    <div class="text-center px-4 sm:px-20 py-24 lg:py-36">
        <h2 class="font-sans text-4xl font-black mb-12 text-white">VaultStack</h2>
        <h4 class="font-sans text-2xl font-semibold mb-12 text-white">Manage your collection of links.</h4>
        <p class="font-sans text-xl font-medium mb-12 text-white">A project for storing, searching, and grouping the collections of URLs that we build up over time, from useful packages to guides on new tools.</p>
        <a href="/vaultstack" class="font-sans text-md font-medium text-white hover:text-primary-500 umami--click--view-vaultstack">View details</a>
    </div>
</div>
