<div class="bg-gradient-to-tr from-primary-800 to-primary-500">
    <div class="max-w-auto mx-auto px-2">
        <div class="flex items-center justify-center">
            <div class="text-center py-16 sm:py-20 lg:py-24">
                <h2 class="text-white text-2xl sm:text-4xl lg:text-6xl pb-4 sm:pb-8 font-sans font-black">Not downloaded my CV yet?</h2>
                <h3 class="text-white text-xl sm:text-2xl lg:text-4xl font-sans font-black">Feel free to grab a copy.</h3>
                <div class="mt-8 sm:mt-12 lg:mt-16">
                    <a href="/assets/downloads/Paul_Quine_CV_2023.pdf" target="_blank" class="px-6 py-3 font-sans text-lg lg:text-2xl font-bold text-white bg-transparent border-2 border-white hover:text-primary-800 hover:bg-white umami--click--download-cv">Download</a>
                </div>
            </div>
        </div>
    </div>
</div>
