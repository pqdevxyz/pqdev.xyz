<div class="container mx-auto px-8 text-center lg:text-left">
    <div>
        <p class="font-sans font-bold text-3xl text-primary-600 mb-2">Software Engineer</p>
        <p class="font-sans font-semibold text-2xl text-neutral-600 mb-2">Bluecrest Health, Worthing</p>
        <p class="font-sans font-medium italic text-2xl text-neutral-600 mb-4">November 2019 - Present</p>
    </div>
    <div class="mb-12 max-w-6xl">
        <p class="font-sans font-medium text-xl text-neutral-500 mb-4 sm:mb-2">Achievements & Responsibilities</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I was part of a team that rebuilt our results system from the ground up, simplifying unnecessary complexity by using service-based architecture along with domain-driven design and event sourcing that has enabled us to scale up the service. This has involved migrating more than 40 million results records into the new system to provide better insights for our customers and staff.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">As part of the new results system, I helped architect and design the trending functionality, this had several challenges that needed solving around the different ways we would trend on customers' results over time, and how they would be presented back to the customer depending on the type of test.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I was the lead developer on a project to migrate our printed reports over to a new print house, this involved us completely changing the process we used to produce and package our health reports, we changed from generating our own PDFs house to providing the required data to our new print house who would deal with the layout and composition their side, this also required rebuilding the process our results team used to track and manage results in our system.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I rebuilt our data anonymisation process reducing the complexity and time to process from 6 hours to 1 hour to anonymise all the PII in our system, helping QA to get fresher data sooner and improving the developer experience with a reduced version of the database for other developers to use locally.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I was part of a team that rebuilt our B2C booking journey during COVID-19 enabling the business to generate new revenue streams during lockdowns.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I built a proof of concept system using Redis as a primary data store, to see if it would be appropriate for our new results system.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I helped in our continuing rollout of IaC (Terraform) to enable us to bring our infrastructure into our VCS (Git).</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">I worked with our DevOps engineer to add additional tooling to our development pipeline, ensuring we deliver higher quality code in less time, e.g. helping to introduce CI into our workflow, and improving our PR process.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed">I worked on adding new features, fixing bugs, and generally improving the day to day use of our internal & external systems.</p>
    </div>
    <div>
        <p class="font-sans font-bold text-3xl text-primary-600 mb-2">Web Developer</p>
        <p class="font-sans font-semibold text-2xl text-neutral-600 mb-2">i2i Media, Haywards Heath</p>
        <p class="font-sans font-medium italic text-2xl text-neutral-600 mb-4">October 2016 - November 2019</p>
    </div>
    <div class="mb-12 max-w-6xl">
        <p class="font-sans font-medium text-xl text-neutral-500 mb-2">Achievements & Responsibilities</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">Rebuilt bespoke CMS from scratch in Laravel to replace an old CMS written in Classic ASP, making it faster and simpler for our clients to manage their content.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed mb-4 sm:mb-2">Built several new websites for high end TV / film clients – from small independent film companies to major Hollywood studios and distributors.</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed">Responsible for creating bespoke marketing & newsletter emails for our clients to increase sales, written in HTML and sent through a custom built Laravel tool which integrates to our bespoke CMS.</p>
    </div>
    <div>
        <p class="font-sans font-bold text-3xl text-primary-600 mb-2">Researcher</p>
        <p class="font-sans font-semibold text-2xl text-neutral-600 mb-2">i2i Media, Haywards Heath</p>
        <p class="font-sans font-medium italic text-2xl text-neutral-600 mb-4">June 2015 – October 2016</p>
    </div>
    <div class="max-w-6xl">
        <p class="font-sans font-medium text-xl text-neutral-500 mb-2">Achievements & Responsibilities</p>
        <p class="font-sans font-medium text-lg text-neutral-600 leading-relaxed">Finding new customers for one of our products, a newsletter about upcoming industry productions.</p>
    </div>
</div>
