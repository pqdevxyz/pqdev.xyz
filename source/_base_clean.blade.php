<!doctype html>
<html lang="{{ $page->language ?? 'en' }}">
    <head>
        <title>@yield('site-title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @stack('meta')
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=2">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=2">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=2">
        <link rel="manifest" href="/assets/favicon/site.webmanifest?v=2">
        <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=2" color="#9333ea">
        <link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=2">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=2">
        <meta name="theme-color" content="#ffffff">
        @include('_includes.styles')
    </head>
    <body>
        <main class="bg-white">
            @yield('content')
        </main>
        @include('_includes.scripts')
    </body>
</html>
