@extends('_base_inverse')
@section('site-title', 'Terraform Project')

@push('meta')
    <meta name="description" content="A project for importing my infrastructure into Terraform, including my DR / failover process.">
    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://pqdev.xyz/terraform" />
    <meta property="og:title" content="Terraform Project" />
    <meta property="og:description" content="A project for importing my infrastructure into Terraform, including my DR / failover process."/>
    <meta property="og:image" content="/assets/images/Logo.png" />
@endpush

@section('content')
    <div class="container mx-auto px-8 py-8">
        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Project</p>
        <h2 class="font-sans text-5xl font-light mb-12 text-primary-600 text-center md:text-left">Terraform (IaC)</h2>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Summary</p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            A project for importing my infrastructure into Terraform, including my DR / fail over process.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Background</p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have been helping to roll out IaC at work in order to bring our infrastructure into VCS, I have been very impressed at how simple and easy it has made making changes and applying them consistently across multiple resources.
        </p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            I decided I would do the same thing for my own infrastructure. I was able to migrate all my current major AWS infrastructure into Terraform as well as setting up Terraform files for my DR / fail over setup.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Experience Gained</p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have gained lots of experience in working with Terraform and how to manage multiple services across an entire account. It has also enabled me to save on cost and make all of my infrastructure consistent in a way that is simply too difficult to do by hand.
        </p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            Having my DR / fail over setup inside my IaC project enables me to spin up new instances in a different region much faster than doing it all manually via the AWS console. Another major benefit is that I can you regular DR practise run through to enable me to iron out any issues with my setup before having to do it for real.
        </p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            One of the more interesting things I have done is setting up my Terraform backend, I have the state file stored in S3 and replicated between two independent regions and a DynamoDB table set up to enable state locking, this is something that I was then able to set up at work to improve our continuing rollout of IaC.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 mb-6 text-center md:text-left">Key Tech Used</p>

        <div class="grid grid-cols-2 md:grid-cols-8 gap-4 mb-12">
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://www.terraform.io/" target="_blank" class="umami--click--terraform-site">
                    <img src="https://content.pqdev.xyz/terraform/terraform.png" title="Terraform">
                </a>
            </div>
        </div>
    </div>
@endsection
