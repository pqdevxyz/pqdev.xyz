@extends('_base')
@section('site-title', 'Paul Quine - Software Engineer')

@push('meta')
    <meta name="description" content="I'm a dedicated software engineer with a comprehensive skill set encompassing DevOps, security, backend and frontend development, and architecture, with a passion for creating secure and efficient software solutions.">
    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://pqdev.xyz/" />
    <meta property="og:title" content="Paul Quine - Software Engineer" />
    <meta property="og:description" content="I'm a dedicated software engineer with a comprehensive skill set encompassing DevOps, security, backend and frontend development, and architecture, with a passion for creating secure and efficient software solutions." />
    <meta property="og:image" content="/assets/images/Logo.png" />
@endpush

@section('content')
    @include('_components.cv-banner')

    <div class="container mx-auto px-8 py-16">
        <p class="font-sans font-extrabold text-4xl text-primary-600 text-center mb-6">About Me</p>
        <p class="font-sans text-neutral-700 text-lg font-medium leading-relaxed text-center">
            I'm a dedicated software engineer with a comprehensive skill set encompassing DevOps, security, backend and frontend development, and architecture, with a passion for creating secure and efficient software solutions. I'm committed to delivering innovative and robust software solutions that drive business success by using my expertise in these different domains and continuous learning and improving my skills as part of my professional growth. My communication skills have proven to be valuable when working with cross-functional teams and collaborating with internal and external stakeholders across the business.
        </p>
    </div>

    <div class="h-2 bg-gradient-to-r from-primary-800 to-primary-500"></div>

    @include('_components.myroi')

    <div class="h-2 bg-gradient-to-l from-primary-800 to-primary-500"></div>

    @include('_components.vaultstack')

    <div class="h-2 bg-gradient-to-r from-primary-800 to-primary-500"></div>

    <div class="container mx-auto px-8 py-16">
        <p class="font-sans font-extrabold text-4xl text-primary-600 text-center">My Other Smaller Projects</p>
    </div>

    @include('_components.other-projects')

    <div class="container mx-auto px-8 py-16">
        <p class="font-sans font-extrabold text-4xl text-primary-600 text-center">My Experience</p>
    </div>

    @include('_components.experience')

    <div class="container mx-auto px-8 py-16">
        <p class="font-sans font-extrabold text-4xl text-primary-600 text-center">My Skills</p>
    </div>

    @include('_components.skills')

    <div class="container mx-auto px-8 py-16 text-center">
        <p class="font-sans font-extrabold text-4xl text-primary-600 mb-4">Contact Me</p>
        <p class="font-sans font-medium text-lg text-neutral-600 mb-4">Linkedin | <a href="https://www.linkedin.com/in/paul-quine-a59375241" class="italic text-primary-500">Paul Quine</a></p>
        <p class="font-sans font-medium text-lg text-neutral-600">Please see my CV for contact information.</p>
    </div>

@endsection
