<nav>
    <div class="container mx-auto py-8">
        <h1 class="font-sans font-bold text-3xl sm:text-6xl text-center text-primary-600 mb-4"><a href="/" class="umami--click--home">Paul Quine</a></h1>
        <p class="font-sans font-bold text-xl sm:text-2xl text-center text-neutral-700"><a href="/" class="umami--click--home">Software Engineer</a></p>
    </div>
</nav>
