<nav class="bg-gradient-to-l from-primary-800 to-primary-500">
    <div class="container mx-auto py-8">
        <h1 class="font-sans font-bold text-3xl sm:text-6xl text-center text-white mb-4"><a href="/" class="umami--click--home">Paul Quine</a></h1>
        <p class="font-sans font-bold text-xl sm:text-2xl text-center text-white"><a href="/" class="umami--click--home">Software Engineer</a></p>
        <div class="mt-6 sm:mt-8 text-center">
            <a href="/" class="px-4 py-2 font-sans text-md lg:text-lg font-bold text-white bg-transparent border-2 border-white hover:text-primary-800 hover:bg-white umami--click--home">Home</a>
        </div>
    </div>
</nav>
