<link href="{{ mix('css/app.css', 'assets/build') }}" rel="stylesheet" type="text/css">
<link href="{{ mix('css/fonts.css', 'assets/build') }}" rel="stylesheet" type="text/css">
@stack('styles')
