@if ($page->production)
    <script async defer src="https://analytics.umami.is/script.js" data-website-id="5405e2f7-e0e8-4a47-9810-299a8515e073"></script>
@endif
@stack('scripts')
