<footer class="bg-gradient-to-l from-primary-800 to-primary-500">
    <div class="container mx-auto py-4 sm:py-8">
        <p class="font-sans font-semibold text-lg sm:text-xl text-center text-white">&copy; {{ date('Y') }} Designed & Built by Paul Quine</p>
    </div>
</footer>
