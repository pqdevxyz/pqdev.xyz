@extends('_base_clean')
@section('site-title', 'Paul Quine - Software Engineer')

@section('content')
    <div class="min-h-screen flex items-center justify-center bg-primary-100 py-12 px-4 sm:px-6 lg:px-8">
        <div>
            <h2 class="font-sans font-semibold text-8xl text-primary-600 text-center mb-6">
                404
            </h2>
            <h4 class="font-sans text-neutral-700 text-4xl leading-relaxed text-center mb-6">
                Not Found
            </h4>
            <p class="font-sans text-neutral-500 text-xl font-light leading-relaxed text-center mb-6">
                We couldn't find the page you are looking for, please check the link and try again.
            </p>
            <div class="mt-8 sm:mt-12 text-center">
                <a href="/" class="px-6 py-3 font-sans bg-primary-500 text-lg lg:text-2xl font-bold text-white border-2 border-primary-500 hover:text-primary-500 hover:bg-transparent umami--click--home">Go Home</a>
            </div>
        </div>
    </div>
@endsection
