@extends('_base_inverse')
@section('site-title', 'MyROI Project')

@push('meta')
    <meta name="description" content="A project for keeping track of investments for small to medium size portfolios.">
    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://pqdev.xyz/myroi" />
    <meta property="og:title" content="MyROI Project" />
    <meta property="og:description" content="A project for keeping track of investments for small to medium size portfolios." />
    <meta property="og:image" content="/assets/images/Logo.png" />
@endpush

@section('content')
    <div class="container mx-auto px-8 py-8">
        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Project</p>
        <h2 class="font-sans text-5xl font-light mb-12 text-primary-600 text-center md:text-left">MyROI</h2>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Summary</p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            A project for keeping track of investments for small to medium size portfolios.
            It uses caching to improve performance when fetching data for more complex calculations.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Background</p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            I wanted a replacement for an excel spreadsheet that I was using to track my investments and manage details about my portfolio. So I built an app for it, where I can create and track all of the individual transactions, as well as using them to calculate additional data like profit/loss.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Experience Gained</p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have gained experience in using Redis to cache data to speed up page load times & reduce the number of queries the application uses, I have been able to test this with Sentry’s APM tool that allows me to measure the performance of each request, which I then used to increase the performance by tackling the slowest requests first.
        </p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have also gained lots more experience working with Tailwind & used it to completely redesign the UI of the application after reading and following along to the Refactoring UI course, it enabled me to better understand how to create good UI & UX that enables users to interact with the underlying application in a clear & intuitive way.
        </p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            This was also the first project that I have set up & used Cypress for my end to end browser testing. I have found it to work very well as an additional tool to Laravel Dusk, which I use to test the visual aspects of each page in turn. However, I use Cypress to run my full end to end test suite, in order to make sure I haven't broken anything with my changes.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 mb-6 text-center md:text-left">Key Tech Used</p>

        <div class="grid grid-cols-2 md:grid-cols-8 gap-4 mb-12">
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://laravel.com/" target="_blank" class="umami--click--laravel-site">
                    <img src="https://content.pqdev.xyz/myroi/tech_logos/laravel.png" title="Laravel">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://tailwindcss.com/" target="_blank" class="umami--click--tailwind-site">
                    <img src="https://content.pqdev.xyz/myroi/tech_logos/tailwind.png" title="Tailwind">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://redis.io/" target="_blank" class="umami--click--redis-site">
                    <img src="https://content.pqdev.xyz/myroi/tech_logos/redis.png" title="Redis">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://www.cypress.io/" target="_blank" class="umami--click--cypress-site">
                    <img src="https://content.pqdev.xyz/myroi/tech_logos/cypress.png" title="Cypress">
                </a>
            </div>
        </div>

        <div class="grid md:grid-cols-2 gap-4 mb-12">
            <a href="https://content.pqdev.xyz/myroi/dashboard.png" target="_blank" class="umami--click--myroi-dashboard-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-dashboard"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/overview.png" target="_blank" class="umami--click--myroi-overview-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-overview"></div>
                    </div>
                </div>
            </a>
        </div>

        <div class="mb-12">
            <a href="https://bitbucket.org/pqdevxyz/myroi/src/master/" target="_blank" class="px-6 py-5 block text-center font-sans text-lg lg:text-2xl font-semibold text-white bg-gradient-to-b from-primary-700 to-primary-500 umami--click--view-myroi-code">View Code</a>
        </div>

        <div class="grid md:grid-cols-3 gap-4 mb-12">
            <a href="https://content.pqdev.xyz/myroi/closed.png" target="_blank" class="umami--click--myroi-closed-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 -translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-closed"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/manage_portfolio.png" target="_blank" class="umami--click--myroi-manage-portfolio-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 -translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-manage_portfolio"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/transaction.png" target="_blank" class="umami--click--myroi-transaction-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 -translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-transaction"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/dark_dashboard.png" target="_blank" class="umami--click--myroi-dark-dashboard-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-dark-dashboard"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/dark_overview.png" target="_blank" class="umami--click--myroi-dark-overview-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-dark-overview"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/myroi/dark_manage_portfolio.png" target="_blank" class="umami--click--myroi-dark-manage-portfolio-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 -translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 myroi-myroi-dark-manage_portfolio"></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
