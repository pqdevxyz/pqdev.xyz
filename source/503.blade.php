@extends('_base_clean')
@section('site-title', 'Paul Quine - Software Engineer')

@section('content')
    <div class="min-h-screen flex items-center justify-center bg-primary-100 py-12 px-4 sm:px-6 lg:px-8">
        <div>
            <h2 class="font-sans font-semibold text-8xl text-primary-600 text-center mb-6">
                503
            </h2>
            <h4 class="font-sans text-neutral-700 text-4xl leading-relaxed text-center mb-6">
                Under Maintenance
            </h4>
            <p class="font-sans text-neutral-500 text-xl font-light leading-relaxed text-center mb-6">
                We are currently carrying out some maintenance. Please come back later.
            </p>
        </div>
    </div>
@endsection
