@extends('_base_inverse')
@section('site-title', 'VaultStack Project')

@push('meta')
    <meta name="description" content="A project for storing, searching, and grouping the collections of URLs that we build up over time, from useful packages to guides on new tools.">
    <meta name="twitter:card" content="summary" />
    <meta property="og:url" content="https://pqdev.xyz/vaultstack" />
    <meta property="og:title" content="VaultStack Project" />
    <meta property="og:description" content="A project for storing, searching, and grouping the collections of URLs that we build up over time, from useful packages to guides on new tools." />
    <meta property="og:image" content="/assets/images/Logo.png" />
@endpush

@section('content')
    <div class="container mx-auto px-8 py-8">
        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Project</p>
        <h2 class="font-sans text-5xl font-light mb-12 text-primary-600 text-center md:text-left">VaultStack</h2>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Summary</p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            A project for storing, searching, and grouping the collections of URLs that we build up over time, from useful packages to guides on new tools.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Background</p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I often find useful links to packages, tutorials, or new tech that I would like to refer back to at a later date, it could be a package that would help me on one of my projects or at work.
        </p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I would save these as bookmarks or just copy and paste the links into slack, this worked but didn't have any way for me to group and search them or just to scan over the content quickly, meaning I would end up having to search for packages or tutorials again and again.
        </p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            So I built an app that enabled me to do just that, I can store the URL along with a description so I can search for it at any point in the future, it can also add entries into groups (e.g. backend, Laravel, security, PHP, etc.) this makes it easy to look through just a subset of the links I have been building up.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 text-center md:text-left">Experience Gained</p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have been able to build the project using a DDD approach, based on the Laravel Beyond CRUD course. This enabled me to break my application code from my domain code with the use of actions & data transfer objects.
        </p>
        <p class="font-sans text-xl font-medium mb-4 text-neutral-800 text-center md:text-left">
            I have been able to continue to build on my previous experience in building good UI / UX that enables users to interact with the underlying application in a clear & intuitive way.
        </p>
        <p class="font-sans text-xl font-medium mb-12 text-neutral-800 text-center md:text-left">
            I also used this project to test out Laravel Octane, which enabled me to replace PHP FPM with openswoole, which is written in mostly C++ and enables much faster performance of PHP code. I have seen a reduction of 40-50% in TTFB compared to PHP FPM.
        </p>

        <p class="font-sans text-lg font-medium uppercase text-neutral-500 mb-6 text-center md:text-left">Key Tech Used</p>

        <div class="grid grid-cols-2 md:grid-cols-8 gap-4 mb-12">
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://laravel.com/" target="_blank" class="umami--click--laravel-site">
                    <img src="https://content.pqdev.xyz/vaultstack/tech_logos/laravel.png" title="Laravel">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://tailwindcss.com/" target="_blank" class="umami--click--tailwind-site">
                    <img src="https://content.pqdev.xyz/vaultstack/tech_logos/tailwind.png" title="Tailwind">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://www.cypress.io/" target="_blank" class="umami--click--cypress-site">
                    <img src="https://content.pqdev.xyz/vaultstack/tech_logos/cypress.png" title="Cypress">
                </a>
            </div>
            <div class="bg-gradient-to-tr from-primary-700 to-primary-500">
                <a href="https://github.com/laravel/octane" target="_blank" class="umami--click--octane-site">
                    <img src="https://content.pqdev.xyz/vaultstack/tech_logos/octane.png" title="Laravel Octane">
                </a>
            </div>
        </div>

        <div class="grid md:grid-cols-2 gap-4 mb-12">
            <a href="https://content.pqdev.xyz/vaultstack/dashboard.png" target="_blank" class="umami--click--vaultstack-dashboard-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 vaultstack-vaultstack-dashboard"></div>
                    </div>
                </div>
            </a>
            <a href="https://content.pqdev.xyz/vaultstack/item.png" target="_blank" class="umami--click--vaultstack-item-image">
                <div class="grid grid-cols-1 grid-rows-1">
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-10 bg-primary-600 opacity-75 translate-x-1 translate-y-1"></div>
                    <div class="col-start-1 col-end-2 row-start-1 row-end-2 z-20">
                        <div class="bg-center bg-cover bg-no-repeat aspect-w-16 aspect-h-9 vaultstack-vaultstack-item"></div>
                    </div>
                </div>
            </a>
        </div>

        <div class="mb-12">
            <a href="https://bitbucket.org/pqdevxyz/vaultstack/src/master/" target="_blank" class="px-6 py-5 block text-center font-sans text-lg lg:text-2xl font-semibold text-white bg-gradient-to-b from-primary-700 to-primary-500 umami--click--view-vaultstack-code">View Code</a>
        </div>
    </div>
@endsection
