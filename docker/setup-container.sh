#!/usr/bin/env bash

cd /var/www/html

echo "127.0.0.1 pqdev.test" >> /etc/hosts

#Install composer packages
composer install -n

#Install yarn packages
yarn install --non-interactive

#Set file permissions
chmod -R 775 .
chmod -R 777 ./docker/nginx/*.crt
chmod -R 777 ./docker/nginx/*.key
