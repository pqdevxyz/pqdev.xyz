#!/usr/bin/env bash

#Install php packages
composer install

#Install yarn packages
yarn install

#Run yarn build
yarn run ${YARN_MODE}

#Copy development files into build folder
cp sitemap.xml ./${BUILD_FOLDER}/sitemap.xml
cp robots.txt.${ENV_MODE} ./${BUILD_FOLDER}/robots.txt
